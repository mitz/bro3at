class Trade
  attr_accessor :account, :logger

  def initialize(account)
    @account = account
    @ua = Browser.new
    @ua.cookie_jar.load cookies_path if File.exists? cookies_path

    @logger = Logger.new $stderr
  end

  def exhibit_10
    pp account

    res = @ua.get exhibitListURL
    t = res.at('.tradeTables')
    exhibitCount = t.search('tr').size - 1

    num_pages = 1
    @ua.get deckURL
    if @ua.page.at('.pager a[@title="last page"]')
      href = @ua.page.at('.pager a[@title="last page"]').attr('href')
      num_pages = URL.new(href).params['p'].to_i
    end
    cards = []
    puts num_pages
    1.upto(num_pages) do |n|
      logger.debug "Fetching deck (page=%d)" % n
      @ua.get deckURL, { :p => n }
      cards += getCardInfo(@ua.page)
    end
    pp cards
    #cards = cards.find { |card| card.rarerity == 'UC' and card.cost != 1.5 }
    #puts cards
  end

  def deckURL
    'http://%s.3gokushi.jp/card/deck.php' % account.server
  end

  def exhibitListURL
    'http://%s.3gokushi.jp/card/exhibit_list.php' %account.server
  end

  def getCardInfo(page)
    cards = page.search('//*[@class="cardStatusDetail"]')
    pp cards.size

    result = []
    cards.each do |c|
      card = {}
      card_id = $1 if c.at('select').attr('name') =~ /\[(.*)\]/
      card['card_id'] = card_id
      card['card_no'] = page.at('//*[@id="cardWindow_%d"]//*[@class="cardno"]' % card_id).text
      card['rarerity'] = page.at('//*[@id="cardWindow_%d"]//span[1]' % card_id).text
      card['name'] = page.at('//*[@id="cardWindow_%d"]//*[@class="name"]' % card_id).text
      card['lv'] = page.at('//*[@id="cardWindow_%d"]//span[4]' % card_id).text
      card['cost'] = page.at('//*[@id="cardWindow_%d"]//*[@class="cost"]' % card_id).text
      card['soltype'] = page.at('//*[@id="cardWindow_%d"]//*[@class="soltype"]/img/@title' % card_id).text
      card['att'] = page.at('//*[@id="cardWindow_%d"]//*[@class="status_att"]' % card_id).text
      card['intl'] = page.at('//*[@id="cardWindow_%d"]//*[@class="status_int"]' % card_id).text
      card['wdef'] = page.at('//*[@id="cardWindow_%d"]//*[@class="status_wdef"]' % card_id).text
      card['sdef'] = page.at('//*[@id="cardWindow_%d"]//*[@class="status_sdef"]' % card_id).text
      card['bdef'] = page.at('//*[@id="cardWindow_%d"]//*[@class="status_bdef"]' % card_id).text
      card['rdef'] = page.at('//*[@id="cardWindow_%d"]//*[@class="status_rdef"]' % card_id).text
      card['speed'] = page.at('//*[@id="cardWindow_%d"]//*[@class="status_speed"]' % card_id).text
      #card['country'] = page.at('//*[@id="cardWindow_%d"]//*[@class="status_country"]/img/@title' % card_id).text
      card['toubatsu'] = page.at('//*[@id="cardWindow_%d"]/..//*[@class="statusParameter1"]//tr[last()]/td' % card_id).text

      result.push card
    end
    result
  end

  def cookies_path
    File.join ::Rails.root.to_s, 'tmp', 'cookies', 'account.%d.txt' % @account.mixi.id
  end
end
