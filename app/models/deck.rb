#-*- coding: utf-8 -*-

require 'browser'

class Deck
  include ActiveModel::AttributeMethods
  include ActiveModel::Validations

  attr_accessor :account, :logger

  def initialize(account)
    @account = account
    @ua = Browser.new
    @ua.cookie_jar.load cookies_path if File.exists? cookies_path

    @logger = Logger.new $stdout
  end

  def login
    @account.login
    # reload cookie
    @ua.cookie_jar.load cookies_path if File.exists? cookies_path
  end

  def list
    result = []
    res = @ua.get deckURL
    cards = res.search(".cardColmn")
    cards.each do |card|
      if d = card.at('a img.aboutdeck')
        onclick = d.attr('onclick')
        if onclick.match(/.*\'(.+?)\', (\d+)?, (.+)?/)
          name = $1
          cardId = $2

          gauge = card.at('dt:contains("討伐ゲージ:")').next.next.at(".para:nth-child(1)").text.strip.to_i
          status = card.at('dt:contains("セット状態:")').next.text.strip
          result += [{ :name => name, :card_id => cardId, :gauge => gauge, :status => status }]
        end
      end
    end

    logger.debug result

    result
  end

  def unset_tired
    threads = []
    g = account.mode == 'BP' ? 100 : 300

    list.each do |card|
      if card[:status].to_s =~ /^待機中/ and card[:gauge] < g
        logger.debug "unset %s(%s), gauge = %d" % [ card[:name], card[:card_id], card[:gauge] ]
        threads.push(Thread.new { unset(card[:card_id]) })
      end
    end
    threads.each { |t| t.join }
  end

  def fill_deck
    @ua.get deckURL

    return if @ua.page.at('.aboutdeck.set_release').nil?

    num_pages = 1
    if @ua.page.at('.pager li:last a')
      pages = @ua.page.search('.pager li a')
      num_pages = pages.map { |p| URL.new(p.attr('href')).params['p'].to_i }.max
    else
      href = @ua.page.search('.pager a').last.attr('href')
      num_pages = URL.new(href).params['p'].to_i
    end
    vill_id = @ua.page.at('#deck_add_selected_village option:first').attr('value') # 本拠地
    cost_now, cost_max = @ua.page.at('.cost .state .volume').text.split(/ \/ /)
    rcost = cost_max.to_f - cost_now.to_f

    cards = []
    1.upto(num_pages) do |n|
      logger.debug "Fetching deck (page=%d)" % n
      @ua.get deckURL, { :p => n }
      cards += getCardInfo(@ua.page)
    end
    cards.sort_by! do |c|
      c['att'].to_f
    end.reverse!

    set_deck vill_id, rcost, cards
  end

  def set_deck(village_id, rcost, cards)
    names = []
    ids = []
    cards.each do |card|
      cost = card['cost'].to_f
      if rcost - cost >= 0 and names.index(card['name']) == nil
        names.push card['name']
        ids.push card['card_id']
        rcost -= cost
      end
    end

    threads = []
    ids.each do |id|
      threads.push(Thread.new do
        logger.debug "set to deck (card_id = %d, village_id = %d)" % [ id, village_id ]

        params = {
          'mode' => 'set',
          'target_card' => id,
          'wild_card_flg' => '',
          'inc_point' => '',
          'btn_change_flg' => '',
          'p' => '',
          'l' => '',
          'ssid' => ssid,
          'selected_village[%d]' % id => village_id,
        }
        @ua.post deckURL, params
      end)
    end

    threads.each { |t| t.join }
  end

  def unset(card_id)
    params = {
      'mode' => 'unset',
      'target_card' => card_id,
      'wild_card_flg' => '',
      'inc_point' => '',
      'btn_change_flg' => '',
      'l' => '',
      'ssid' => ssid,
    }
    @ua.post deckURL, params
  end

  def getCardInfo(page)
    cards = page.search('#cardFileList .aboutdeck.set_release')
    result = []
    cards.each do |c|
      card = {}
      card_id = $1 if c.attr('onclick') =~ /(\d+)/
      #card_id = $1 if c.at('select').attr('name') =~ /\[(.*)\]/
      card['card_id'] = card_id
      card['card_no'] = page.at('#cardWindow_%d .cardno' % card_id).text
      card['rarerity'] = page.at('#cardWindow_%d span:nth-child(1) img' % card_id).attr("title")
      card['name'] = page.at('#cardWindow_%d .name' % card_id).text
      card['lv'] = page.at('#cardWindow_%d .level span' % card_id).text
      card['cost'] = page.at('#cardWindow_%d .cost' % card_id).text
      card['soltype'] = page.at('#cardWindow_%d .soltype img' % card_id).attr('title')
      card['att'] = page.at('#cardWindow_%d .status_att' % card_id).text
      card['intl'] = page.at('#cardWindow_%d .status_int' % card_id).text
      card['wdef'] = page.at('#cardWindow_%d .status_wdef' % card_id).text
      card['sdef'] = page.at('#cardWindow_%d .status_sdef' % card_id).text
      card['bdef'] = page.at('#cardWindow_%d .status_bdef' % card_id).text
      card['rdef'] = page.at('#cardWindow_%d .status_rdef' % card_id).text
      card['speed'] = page.at('#cardWindow_%d .status_speed' % card_id).text
      #card['country'] = page.at('//*[@id="cardWindow_%d"]//*[@class="status_country"]/img/@title' % card_id).text
      card['toubatsu'] = page.at('//*[@id="cardWindow_%d"]/..//*[@class="statusParameter1"]//tr[last()]/td' % card_id).text

      if account.mode == 'TP'
        if card['toubatsu'].to_i == 300 and card['cost'].to_f >= 1.5 and card['cost'].to_f <= 3.0 and ["UC", "R" ].include? card['rarerity']
          result.push card
        end
      else
        if card['cost'].to_f >= 1.5 and card['cost'].to_f <= 3.0 and ["UC", "R"].include? card['rarerity']
          result.push card
        end
      end
    end
    result
  end

  def troop_all
    mode = account.mode || 'TP'
    if mode == 'TP'
      troop_all_gauge500
    elsif mode == 'BP'
      troop_all_gauge100
    end
  end

  def troop_all_gauge500
    user = User.new account
    bc = user.basecamp
    target = user.closest_territory

    threads = []
    list.each do |card|
      logger.debug "%s(%s), gauge = %d" % [ card[:name], card[:card_id], card[:gauge] ]
      if card[:status].to_s =~ /^待機中/ and card[:gauge] == 500
        logger.debug "troop %s(%s), gauge = %d" % [ card[:name], card[:card_id], card[:gauge] ]
        threads.push(Thread.new { troop(target, card[:card_id]) })
      end
    end
    threads.each { |t| t.join }
  end

  def troop_all_gauge100
    user = User.new account
    bc = user.basecamp
    target = user.closest_territory

    threads = []
    list.each do |card|
      logger.debug "%s(%s), gauge = %d" % [ card[:name], card[:card_id], card[:gauge] ]
      if card[:status].to_s =~ /^待機中/ and card[:gauge] >= 100
        logger.debug "troop %s(%s), gauge = %d" % [ card[:name], card[:card_id], card[:gauge] ]
        threads.push(Thread.new { troop(target, card[:card_id]) })
      end
    end
    threads.each { |t| t.join }
  end

  def troop(target, card_id)
    params = {
      :village_x_value => target[0],
      :village_y_value => target[1],
      :village_name => '',
      :card_id => 212,
      :radio_move_type => 302, # 殲滅戦
      :show_beat_bandit_flg => 1,
      :unit_assign_card_id => card_id,
      :btn_send => '出兵',
    }

    @ua.post troopURL, params
  end

  def ssid
    @ua.page.at('[@name=ssid]').attr('value')
  end

  def deckURL
    'http://%s.3gokushi.jp/card/deck.php' % account.server
  end

  def troopURL
    "http://%s.3gokushi.jp/facility/castle_send_troop.php" % account.server
  end

  def cookies_path
    File.join ::Rails.root.to_s, 'tmp', 'cookies', 'account.%d.txt' % @account.mixi.id
  end

  def ua
    @ua
  end
end
