#-*- coding: utf-8 -*-
#
require 'browser'

class Mixi < ActiveRecord::Base
  attr_accessible :email, :name, :password

  attr_reader :ua

  validates :name,  :presence => true
  validates :email, :presence => true, :email => true

  class LoginError < Exception; end

  def login
    retry_count = 3
    @ua = Browser.new
    @ua.cookie_jar.load cookies_path if File.exists? cookies_path
    begin

      test_login
    rescue LoginError => e
      retry_count -= 1
      if retry_count > 0
        do_login
        retry
      end
    ensure
      FileUtils.mkdir_p File.dirname(cookies_path)
      @ua.cookie_jar.save_as cookies_path
    end

     if @ua.page.title =~ /ブラウザ三国志/
       return true
     else
       return false
     end
  end

  def test_login
    doc = @ua.get 'http://mixi.jp/run_appli.pl?id=6598'
    if doc.uri.path == '/redirect_next_url.pl'
      raise LoginError, "Login failed"
    else
      return true
    end
  end

  def do_login
    @ua.page.form_with :name => 'login_form'  do |form|
      form.field_with(:name => 'email').value = email
      form.field_with(:name => 'password').value = password
      form.checkbox_with(:name => 'sticky').check
      form.submit
    end
    puts @ua.page.content
  end

  def cookies_path
    File.join ::Rails.root.to_s, 'tmp', 'cookies', 'account.%d.txt' % id
  end
end
