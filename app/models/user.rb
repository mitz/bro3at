# -*- encoding: utf-8 -*-

require 'browser'

class User
  include ActiveModel::Validations

  attr_accessor :account, :logger, :ua

  def initialize(account)
    @account = account
    @ua = Browser.new
    @ua.cookie_jar.load cookies_path if File.exists? cookies_path

    @logger = Logger.new $stderr
  end

  def basecamp
    @ua.get profileURL

    t = @ua.page.at('.commonTables tr:nth-child(19) td:nth-child(2)').text
    x, y = t.split(/,/)
    return [x.to_i, y.to_i]
  end

  def tp
    @ua.get profileURL
    begin
      dummy, bp, tp, cp = @ua.page.search('//*[@id="status_point"]/text()')
      account.bp = bp.text.strip.match(/\d+/).to_s.to_i
      account.tp = tp.text.strip.match(/\d+/).to_s.to_i
      account.cp = cp.text.strip.match(/\d+/).to_s.to_i
      account.save
    rescue NoMethodError => e
      logger.error "Failed to update tp (%s @ %s)" % [ account.server, account.mixi.name ]
      logger.error e
    end
  end

  def closest_territory
    # 本拠地の座標
    bc = basecamp

    @ua.get territoryURL

    teritories = @ua.page.search('#all table tr')
    closest = nil
    min = 9999
    teritories.each do |t|
      next if t.at('th') or t.children.empty?
      xy = t.at('td:nth-child(2) a').text
      x, y = xy.split(/,/)
      x, y = x.to_i, y.to_i
      xy2 = [ x, y ]

      d = distance(bc, xy2)
      if d == 1.0
        min = 1.0
        closest = xy2
        break
      else
        if d == [min, d].min
          min = d
          closest = xy2
        end
      end
    end
    return closest
  end

  def distance(a, b)
    Math.sqrt(((a[0] - b[0]) ** 2) + ((a[1] - b[1]) ** 2))
  end

  def yorozu
    logger.info "yorozu (%s@%s)" % [ account.server, account.mixi.name ]
    @ua.get yorozuURL
    counts = @ua.page.search '.sysMes strong'
    r, m = counts
    r = r.text.to_i
    m = m.text.to_i
    params = { 'send' => 'send', 'got_type' => 0 }
    if r > 0
      1.upto r do
        @ua.post yorozuURL, params
      end
    end
    r
  end

  def take_quest_all
    logger.info "take_quest_all (%s@%s)" % [ account.server, account.mixi.name ]
    [ 254, 255, 256 ].each do |i|
      take_quest i
    end
  end

  def take_quest(qid)
    @ua.get takeQuestURL(qid)
  end

  def get_reward_all
    loop do
      logger.info "get_reward_all (%s@%s)" % [ account.server, account.mixi.name ]
      @ua.get questURL
      if @ua.page.search('a[title=報酬を受け取る]').size >= 1
        @ua.get questRewardURL
      else
        break
      end
    end
  end

  def get_reward
    @ua.page.search 'gnavi_questnew'
  end

  def kifu
    logger.info "kifu (%s@%s)" % [ account.server, account.mixi.name ]
    params = { 'wood' => '', 'stone' => '', 'iron' => '', 'rice' => '500', 'contribution' => '1' }
    @ua.post kifuURL, params
  end

  def entry_duel
    @ua.get duelSetURL
    button = @ua.page.search "#duelEntryButton"
    if button.attr 'onclick'
      page = @ua.page.form_with(:name => "deck_file") do |form|
        form.mode = "entry"
        form.target_card = 0
      end.submit
    end
  end

  def assist_all
    @ua.get allianceVillageURL
    gauge = @ua.page.at('.support-gauge-frame p')
    return if gauge.nil?

    g, m =  gauge.text.split ' / '
    count = @ua.page.search ".support-count ul li img[title='助力済み']"
    if g.to_i < 500 and count.size > 0
      1.upto count.size do
        @ua.get assistURL
      end
    end
  end

  def territoryURL
    'http://%s.3gokushi.jp/facility/territory_status.php?p=1&s=2&o=0&sort_order=CODE_TERRITORY_SORT_ORDER_SCORE&sort_order_type=0' % account.server
  end

  def yorozuURL
    'http://%s.3gokushi.jp/reward_vendor/reward_vendor.php' % account.server
  end

  def takeQuestURL(qid)
    'http://%s.3gokushi.jp/quest/index.php?action=take_quest&id=%d' % [ account.server, qid ]
  end

  def questURL
    'http://%s.3gokushi.jp/quest/' % account.server
  end

  def questRewardURL
    'http://%s.3gokushi.jp/quest/index.php?c=1' % account.server
  end

  def kifuURL
    'http://%s.3gokushi.jp/alliance/level.php' % account.server
  end

  def duelSetURL
    'http://%s.3gokushi.jp/card/duel_set.php' % account.server
  end

  def profileURL
    'http://%s.3gokushi.jp/user/' % account.server
  end

  def allianceVillageURL
    'http://%s.3gokushi.jp/alliance/village.php' % account.server
  end

  def assistURL
    'http://%s.3gokushi.jp/alliance/village.php?assist=1' % account.server
  end

  def cookies_path
    File.join ::Rails.root.to_s, 'tmp', 'cookies', 'account.%d.txt' % @account.mixi.id
  end
end
