# encoding: utf-8

require 'url'
require 'browser'
require 'kaminari'

class Bro3 < ActiveRecord::Base
  attr_accessible :bp, :cp, :mixi_id, :server, :mode, :tp, :active

  validates_uniqueness_of :server, :scope => :mixi_id

  paginates_per 20

  belongs_to :mixi

  def login
    res = ua.get deckURL
    if res.uri.path == '/card/deck.php'
      return true
    end

    found = false
    if mixi.login

      ua.cookie_jar.load cookies_path if File.exists? cookies_path
      ua.get 'http://mixi.jp/run_appli.pl?id=6598'

      begin
        iframe_postkey = $1 if ua.page.content =~ /\"iframe_postkey\":\"(.+?)\"/

        iframe = "http://m0.3gokushi.jp/start_mixi.php"
        params = ua.page.at('#application_canvas')["data-runtime-params"]

        headers = {
          'Content-Type' => 'application/x-www-form-urlencoded',
          'Origin' => 'http://mixi.jp',
          'Referer' => 'http://mixi.jp/run_appli.pl?id=6598'
        }
        res = ua.post "http://m0.3gokushi.jp/start_mixi.php", params, headers

        params = 'response_type=code&client_id=mixiapp-web_6598&scope=mixi_apps2%20r_profile&' + 
          'display=iframe_pc&' +
          'iframe_postkey=' + iframe_postkey + '&' +
          'app_id=6598&state=select_server'

        a = "https://mixi.jp/connect_authorize.pl?" + params
        res = ua.post a, {}, headers
        uri = URL.new res.uri.to_s
        code = $1 if uri.hash =~ /code=(.+?)$/

        ua.get 'http://m0.3gokushi.jp/user/callback_mixi.php?state=select_server&code=' + code
        ua.page.search('#serverList a').each do |link|
          if link.attributes["title"].to_s == server + "ワールド"
            a = link["onclick"].split /', '/
            params = {
              'ts' => a[1],
              'p'  => a[2],
              'cd' => a[3],
              'ch' => 'm',
              'ct' => a[5]
            }
            dummy, host = a[0].split /'/
            res = ua.get host + "user/first_login.php", params
            found = true
          end
        end

      ensure
        ua.cookie_jar.save_as cookies_path
      end

      return found
    else
      return false
    end
  end

  def deck
    ua.get server_path + '/card/deck.php'
  end

  def deckURL
    server_path + '/card/deck.php'
  end

  def server_path
    'http://' + server + '.3gokushi.jp'
  end

  def ua
    if @ua.nil?
      @ua ||= Browser.new
      @ua.cookie_jar.load cookies_path if File.exists? cookies_path
    end
    return @ua
  end

  def cookies_path
    File.join ::Rails.root.to_s, 'tmp', 'cookies', 'account.%d.txt' % mixi.id
  end
end
