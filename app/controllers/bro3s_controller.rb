class Bro3sController < ApplicationController
  # GET /bro3s
  # GET /bro3s.json
  def index
    @bro3s = Bro3.page(params[:page])
    @bro3s = @bro3s.where('server = :server', { :server => params[:server] }) if params[:server]

    @num = @bro3s.inject(0) { |sum, account| sum += account.tp / 499 unless account.tp.nil? } || 0

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @bro3s }
    end
  end

  # GET /bro3s/1
  # GET /bro3s/1.json
  def show
    @bro3 = Bro3.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @bro3 }
    end
  end

  # GET /bro3s/new
  # GET /bro3s/new.json
  def new
    @bro3 = Bro3.new
    @bro3.server = params[:server]

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @bro3 }
    end
  end

  # GET /bro3s/1/edit
  def edit
    @bro3 = Bro3.find(params[:id])
  end

  # POST /bro3s
  # POST /bro3s.json
  def create
    @bro3 = Bro3.new(params[:bro3])

    respond_to do |format|
      if @bro3.save
        format.html { redirect_to @bro3, notice: 'Bro3 was successfully created.' }
        format.json { render json: @bro3, status: :created, location: @bro3 }
      else
        format.html { render action: "new" }
        format.json { render json: @bro3.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /bro3s/1
  # PUT /bro3s/1.json
  def update
    @bro3 = Bro3.find(params[:id])

    respond_to do |format|
      if @bro3.update_attributes(params[:bro3])
        format.html { redirect_to @bro3, notice: 'Bro3 was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @bro3.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /bro3s/1
  # DELETE /bro3s/1.json
  def destroy
    @bro3 = Bro3.find(params[:id])
    @bro3.destroy

    respond_to do |format|
      format.html { redirect_to bro3s_url }
      format.json { head :no_content }
    end
  end

end
