class MixisController < ApplicationController
  # GET /mixis
  # GET /mixis.json
  def index
    @mixis = Mixi.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @mixis }
    end
  end

  # GET /mixis/1
  # GET /mixis/1.json
  def show
    @mixi = Mixi.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @mixi }
    end
  end

  # GET /mixis/new
  # GET /mixis/new.json
  def new
    @mixi = Mixi.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @mixi }
    end
  end

  # GET /mixis/1/edit
  def edit
    @mixi = Mixi.find(params[:id])
  end

  # POST /mixis
  # POST /mixis.json
  def create
    @mixi = Mixi.new(params[:mixi])

    respond_to do |format|
      if @mixi.save
        format.html { redirect_to @mixi, notice: 'Mixi was successfully created.' }
        format.json { render json: @mixi, status: :created, location: @mixi }
      else
        format.html { render action: "new" }
        format.json { render json: @mixi.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /mixis/1
  # PUT /mixis/1.json
  def update
    @mixi = Mixi.find(params[:id])

    respond_to do |format|
      if @mixi.update_attributes(params[:mixi])
        format.html { redirect_to @mixi, notice: 'Mixi was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @mixi.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /mixis/1
  # DELETE /mixis/1.json
  def destroy
    @mixi = Mixi.find(params[:id])
    @mixi.destroy

    respond_to do |format|
      format.html { redirect_to mixis_url }
      format.json { head :no_content }
    end
  end
end
