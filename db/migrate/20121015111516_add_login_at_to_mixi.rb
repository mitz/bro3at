class AddLoginAtToMixi < ActiveRecord::Migration
  def change
    add_column :mixis, :login_at, :timestamp
  end
end
