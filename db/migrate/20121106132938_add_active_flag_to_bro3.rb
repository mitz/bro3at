class AddActiveFlagToBro3 < ActiveRecord::Migration
  def up
    change_table :bro3s do |t|
      t.boolean :active
    end
    say_with_time 'Setting active flag' do
      Bro3.update_all("active = 't'")
    end
  end

  def down
    remove_column :bro3s, :active
  end
end
