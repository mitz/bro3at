class CreateBro3s < ActiveRecord::Migration
  def change
    create_table :bro3s do |t|
      t.integer :mixi_id
      t.string :server
      t.integer :bp
      t.integer :tp
      t.integer :cp

      t.timestamps
    end
  end
end
