# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

servers = %w(m1 m9 m17 m25 m29 m33 m37 m39 m41 m43 m44 m45 m46 m47)
servers.each do |s|
  Server.create(:name => s)
end
