#!/usr/bin/evn ruby
#
APP_PATH = File.expand_path('../config/application',  __FILE__)
require File.expand_path('../config/boot',  __FILE__)

require 'rubygems/dependency_installer'

def install_bundle
  inst = Gem::DependencyInstaller.new
  begin
    inst.install 'bundler', '~> 1.2.1'
  rescue Exception => e
    puts e
    exit(1)
  end
end

def bundle_command(command)
  print `"#{Gem.ruby}" -rubygems "#{Gem.bin_path('bundler', 'bundle')}" #{command}`
end

def run_bundle
  bundle_command('install --path=vendor/bundle --quiet')
end

def migrate_db
  bundle_command('exec rake db:migrate')
  bundle_command('exec rake db:seed')
end

def setup
  install_bundle
  run_bundle

  migrate_db
end

def server
  ARGV << 'server'
  ARGV << '-p'
  ARGV << '3594'
  require 'rails/commands'
end

def main
  setup
end

main
