require "spec_helper"

describe MixisController do
  describe "routing" do

    it "routes to #index" do
      get("/mixis").should route_to("mixis#index")
    end

    it "routes to #new" do
      get("/mixis/new").should route_to("mixis#new")
    end

    it "routes to #show" do
      get("/mixis/1").should route_to("mixis#show", :id => "1")
    end

    it "routes to #edit" do
      get("/mixis/1/edit").should route_to("mixis#edit", :id => "1")
    end

    it "routes to #create" do
      post("/mixis").should route_to("mixis#create")
    end

    it "routes to #update" do
      put("/mixis/1").should route_to("mixis#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/mixis/1").should route_to("mixis#destroy", :id => "1")
    end

  end
end
