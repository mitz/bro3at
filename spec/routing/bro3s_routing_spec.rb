require "spec_helper"

describe Bro3sController do
  describe "routing" do

    it "routes to #index" do
      get("/bro3s").should route_to("bro3s#index")
    end

    it "routes to #new" do
      get("/bro3s/new").should route_to("bro3s#new")
    end

    it "routes to #show" do
      get("/bro3s/1").should route_to("bro3s#show", :id => "1")
    end

    it "routes to #edit" do
      get("/bro3s/1/edit").should route_to("bro3s#edit", :id => "1")
    end

    it "routes to #create" do
      post("/bro3s").should route_to("bro3s#create")
    end

    it "routes to #update" do
      put("/bro3s/1").should route_to("bro3s#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/bro3s/1").should route_to("bro3s#destroy", :id => "1")
    end

  end
end
