require 'spec_helper'

describe "mixis/edit" do
  before(:each) do
    @mixi = assign(:mixi, stub_model(Mixi,
      :name => "MyString",
      :email => "MyString",
      :password => "MyString"
    ))
  end

  it "renders the edit mixi form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => mixis_path(@mixi), :method => "post" do
      assert_select "input#mixi_name", :name => "mixi[name]"
      assert_select "input#mixi_email", :name => "mixi[email]"
      assert_select "input#mixi_password", :name => "mixi[password]"
    end
  end
end
