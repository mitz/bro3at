require 'spec_helper'

describe "mixis/index" do
  before(:each) do
    assign(:mixis, [
      stub_model(Mixi,
        :name => "Name",
        :email => "Email",
        :password => "Password"
      ),
      stub_model(Mixi,
        :name => "Name",
        :email => "Email",
        :password => "Password"
      )
    ])
  end

  it "renders a list of mixis" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Email".to_s, :count => 2
    assert_select "tr>td", :text => "Password".to_s, :count => 2
  end
end
