require 'spec_helper'

describe "mixis/show" do
  before(:each) do
    @mixi = assign(:mixi, stub_model(Mixi,
      :name => "Name",
      :email => "Email",
      :password => "Password"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    rendered.should match(/Email/)
    rendered.should match(/Password/)
  end
end
