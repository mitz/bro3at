require 'spec_helper'

describe "bro3s/edit" do
  before(:each) do
    @bro3 = assign(:bro3, stub_model(Bro3,
      :mixi_id => 1,
      :server => "MyString",
      :bp => 1,
      :tp => 1,
      :cp => 1
    ))
  end

  it "renders the edit bro3 form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => bro3s_path(@bro3), :method => "post" do
      assert_select "input#bro3_mixi_id", :name => "bro3[mixi_id]"
      assert_select "input#bro3_server", :name => "bro3[server]"
      assert_select "input#bro3_bp", :name => "bro3[bp]"
      assert_select "input#bro3_tp", :name => "bro3[tp]"
      assert_select "input#bro3_cp", :name => "bro3[cp]"
    end
  end
end
