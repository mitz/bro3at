require 'spec_helper'

describe "bro3s/show" do
  before(:each) do
    @bro3 = assign(:bro3, stub_model(Bro3,
      :mixi_id => 1,
      :server => "Server",
      :bp => 2,
      :tp => 3,
      :cp => 4
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    rendered.should match(/Server/)
    rendered.should match(/2/)
    rendered.should match(/3/)
    rendered.should match(/4/)
  end
end
