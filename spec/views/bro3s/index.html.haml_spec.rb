require 'spec_helper'

describe "bro3s/index" do
  before(:each) do
    assign(:bro3s, [
      stub_model(Bro3,
        :mixi_id => 1,
        :server => "Server",
        :bp => 2,
        :tp => 3,
        :cp => 4
      ),
      stub_model(Bro3,
        :mixi_id => 1,
        :server => "Server",
        :bp => 2,
        :tp => 3,
        :cp => 4
      )
    ])
  end

  it "renders a list of bro3s" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "Server".to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => 4.to_s, :count => 2
  end
end
