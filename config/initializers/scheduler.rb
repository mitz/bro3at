require 'kaminari'

$scheduler = Rufus::Scheduler.start_new

#$scheduler.every '1h', :first_in => '1m' do
#  Mixi.all.each do |mixi|
#    mixi.login
#  end
#end

#$scheduler.every '15m', :first_in => '5s' do
#  start = Time.now
#  Rails.logger.info "Login jobs started at " + start.to_s
#
#  Bro3.all(:include => :mixi).each do |bro3|
#    bro3.login
#    Rails.logger.info "Login successful(%s@%s)" % [bro3.server, bro3.mixi.name]
#  end
#  Rails.logger.info "Login jobs took " + (Time.now - start).to_s
#end

$shlog =  Logger.new 'log/runner.log', 'daily'
$shlog.formatter = Logger::Formatter.new
$shlog.datetime_format = "%Y-%m-%d %H:%M:%S"

unless Module.const_defined? :IRB
  $scheduler.every '15m', :first_in => '5s' do
    Bro3.where('active = ?', true).each do |account|
      begin
        account.login
        deck = Deck.new(account)
        deck.unset_tired
        deck.fill_deck
        deck.troop_all

        user = User.new account
        user.tp
      rescue Exception => e
        $shlog.error 'Error %s@%s' % [ account.server, account.mixi.name ]
        $shlog.error e
        # skip
      end
    end
    ActiveRecord::Base.connection.close
  end

  $scheduler.every '1h', :first_in => '1m' do
    Bro3.all.each do |account|
      begin
        account.login
        user = User.new account
        user.take_quest_all
        user.kifu
        user.get_reward_all
        user.yorozu
        user.assist_all
        user.tp
      rescue Exception => e
        $shlog.error 'Error %s@%s' % [ account.server, account.mixi.name ]
        $shlog.error e
      end
    end
  end

  $scheduler.cron '5 5 * * *' do
    Bro3.all.each do |account|
      begin
        account.login
        user = User.new account
        user.take_quest_all
        user.kifu
      rescue Exception => e
        $shlog.error 'Error %s@%s' % [ account.server, account.mixi.name ]
        $shlog.error e
      end
    end
  end

end
