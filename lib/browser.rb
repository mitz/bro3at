require 'mechanize'

class Browser < Mechanize
  def initialize
    super
    @agent.user_agent = AGENT_ALIASES['Mac FireFox']
  end
end
